DEPENDS:append = " e2fsprogs-native  efitools-native  coreutils-native "
do_configure[depends] += "gnulib:do_populate_sysroot"

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

# Overwrite poky side SRC_URI to remove all security etc patches
# since we update to a newer version anyway and the patches don't apply
SRC_URI = "git://git.savannah.gnu.org/git/grub.git;protocol=https;branch=master"

SRCREV = "7259d55ffcf124e32eafb61aa381f9856e98a708"
PV = "2.06+git${SRCPV}"
S = "${WORKDIR}/git"

SRC_URI[sha256sum] = "a52e73e42dabbda0f9032ef30a5afae00e80abb745cc5c356e3b56fda0048e1d"

do_configure[depends] += "gnulib-native:do_populate_sysroot \
                          ${MLPREFIX}gnulib:do_populate_sysroot"

SRC_URI += "file://grub-initial.cfg \
            file://0001-verifiers-Don-t-return-error-for-deferred-image.patch \
            file://0002-grub-arm-fix-check-for-float-point.patch \
            file://0001-grub-efi-modinfo.sh.in-remove-build-environment-deta.patch \
"

SRC_URI += "file://uefi-certificates/db.key"
SRC_URI += "file://uefi-certificates/db.crt"

GRUB_BUILDIN = "part_gpt fat ext2 configfile pgp gcry_sha512 gcry_rsa \
                password_pbkdf2 echo normal linux all_video \
                search search_fs_uuid reboot sleep"

include grub_armv7_float.inc

do_configure:prepend() {
        cd ${S}

        rm -rf ${S}/gnulib
        cp -rf ${STAGING_DATADIR}/gnulib ${S}/gnulib

        ./bootstrap --gnulib-srcdir=./gnulib
        cd -
}

do_mkimage() {
        cd ${B}

        grub-mkstandalone --disable-shim-lock \
            --format=${GRUB_TARGET}-efi \
            --locale-directory=/usr/share/locale/ \
            --directory=./grub-core/ \
            --modules="${GRUB_BUILDIN}" \
            --output=./${GRUB_IMAGE_PREFIX}${GRUB_IMAGE} \
            "boot/grub/grub.cfg=${WORKDIR}/grub-initial.cfg"

	${STAGING_BINDIR_NATIVE}/sbsign \
            --key ${WORKDIR}/uefi-certificates/db.key \
            --cert ${WORKDIR}/uefi-certificates/db.crt \
            ${GRUB_IMAGE_PREFIX}${GRUB_IMAGE} \
            --output ${GRUB_IMAGE_PREFIX}${GRUB_IMAGE}.signed
	cp ${GRUB_IMAGE_PREFIX}${GRUB_IMAGE}.signed ${GRUB_IMAGE_PREFIX}${GRUB_IMAGE}
}

