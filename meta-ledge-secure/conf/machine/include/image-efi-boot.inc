# SPDX-License-Identifier: MIT

IMAGE_EFI_BOOT_FILES += "default.pcr.signature"
IMAGE_EFI_BOOT_FILES += "kernel515.pcr.signature"
IMAGE_EFI_BOOT_FILES += "ledge-initramfs-${MACHINE}.rootfs.cpio.gz"

XEN_IMAGE_EFI_BOOT_FILES = "${@bb.utils.contains('DISTRO_FEATURES', 'xen', \
                                              'xen-${MACHINE}.efi.signed;xen.efi xen.cfg', '', d)}"
XEN_IMAGE_EFI_BOOT_FILES:ledge-qemuarm = ""
IMAGE_EFI_BOOT_FILES += "${XEN_IMAGE_EFI_BOOT_FILES}"

do_image_wic[depends] += "${@bb.utils.contains('DISTRO_FEATURES', 'xen', \
                                               'xen:do_deploy', '', d)}"

ROOT_PART_UUID = "f3374295-b635-44af-90b6-3f65ded2e2e4"
ROOT_FS_UUID = "6091b3a4-ce08-3020-93a6-f755a22ef03b"
WKS_ROOTFS_PART_EXTRA_ARGS = "--uuid=${ROOT_PART_UUID} --fsuuid=${ROOT_FS_UUID}"

GRUB_CFG_FILE = "${WORKDIR}/grub.cfg"

do_image_wic[prefuncs] += "generate_grub_cfg"

generate_grub_cfg() {
	cat <<-'EOF' > "${GRUB_CFG_FILE}"
	set term="vt100"
	set default="0"
	set timeout="5"

	kernel_cmdline="rootwait rw panic=60"

	rootpart_uuid="${ROOT_FS_UUID}"

	menuentry 'TRS' {
	        echo 'Loading Linux ...'
	        linux /${KERNEL_IMAGETYPE} $kernel_cmdline root=UUID=$rootpart_uuid
	        echo 'Loading initial ramdisk ...'
	        initrd /ledge-initramfs-${MACHINE}.rootfs.cpio.gz
	}
	EOF

	if [ "${@bb.utils.contains('DISTRO_FEATURES', 'xen', '1', '0', d)}" = "1" ] ; then
		cat <<-'EOF' >> "${GRUB_CFG_FILE}"

		menuentry 'TRS Xen (if supported)' {
		        chainloader /xen.efi
		}
		EOF
	fi
}
